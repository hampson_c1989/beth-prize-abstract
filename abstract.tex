\documentclass[11pt]{article}

\newcounter{chapter}
\usepackage{preamble,tablefootnote}

\newcommand{\comment}[1]{}

\begin{document}
\noindent
{\bf Christopher Hampson:}

\smallskip
\begin{center}
{\Large\bf Two-dimensional Modal Logics with Difference Relations}
%\title{Two-dimensional Modal Logics with Difference Relations}
%\author{Christopher Hampson}
%\maketitle

\bigskip
10-page abstract
\end{center}

\section{Background}

Modal logic has a rich history originating in the classical considerations of philosophers such as Aristotle. Originally conceived as the logic of necessary and contingent truths (so-called \emph{alethic logic}), further study has shown the same underlying semantics to encapsulate a vast range of other natural linguistic phenomena; such disparate phenomena as \emph{spatial} and \emph{temporal} reasoning, \emph{epistemic} and \emph{doxastic} reasoning --- relating to one's knowledge and belief, and \emph{deontic} reasoning of one's obligations and permissions~\cite{BdRV01,Goldblatt2003}.\

While there are many applications of modal logics working in isolation, it is often their interactions in which we are most interested. 
%
The notion of \emph{products} of propositional modal logics, introduced by Segerberg~\cite{Segerberg1973} and Shehtman~\cite{Shehtman1978}, provides a conceptually appealing approach to combining modal logics. 
 Products of modal logics are connected to several other `many-dimensional' logical formalisms, such as finite variable fragments of classical, intuitionistic, modal, and temporal first-order logic, as well as temporal epistemic logics~\cite{Fagin1995}, and extensions of description logics with `dynamic' features~\cite{Baader1995a,Wolter1998,Wolter1999,Wolter&Z2000}. 
The product construction has been studied extensively since its inception~\cite{MarxVenema1997,GKWZ03, Kurucz2007}, and has borne many applications in computer science and artificial intelligence~\cite{Reif1983,Baader1995,Finger1999}.

%****

\subsection{Modal product logics -- the definition}

%\begin{definition}
	Given two frames $\F_h=(W_h,R_h)$ and $\F_v=(W_v,R_v)$, we define their \emph{product frame}\index{product frame} to be the $2$-frame
	\begin{equation*}
		\F_h\times \F_v\ :=\ \big(W_h\times W_v,\bar{R}_h,\bar{R}_v\big),
	\end{equation*}
where $W_h\times W_v=\{(x,y) : x\in W_h \mbox{ and } y\in W_v\}$ is the Cartesian product of $W_h$ and $W_v$ and, for all $x,x'\in W_h$ and $y,y'\in W_v$, we have that
\begin{eqnarray*}
	(x,y) \bar{R}_h (x',y') & \iff & x R_h x' \mbox{ and } y=y',\\[3pt]
	(x,y) \bar{R}_v(x',y') & \iff & x = x' \mbox{ and } y R_v y'.
\end{eqnarray*}
%
The subscripts $h$ and $v$ betray the geometric intuition behind this construction, illustrated in Figure~\ref{fig:products}; with the $\bar{R}_h$ denoting the \emph{`horizontal'} accessibility relation and the $\bar{R}_v$ denoting the \emph{`vertical'} accessibility relation. 

\begin{figure}[ht!]
\begin{center}
\includegraphics[scale=0.8]{products.pdf}
\end{center}
\caption{Illustration of the product construction.}
\label{fig:products}
\end{figure}

%\end{definition}

%Given two classes of frames $\C_h$ and $\C_v$, we may define their \emph{product class} $\C_h\times\C_v$ to be the class of all product frames $\F_h\times \F_v$, where $\F_i\in \C_i$, for $i=h,v$. 
%
We consider the bimodal language whose formulas are defined by
%
\[
\varphi:=\ p\ \mid\ \neg\varphi\ \mid\ \varphi_1\land\varphi_2\ \mid\ \Diamond_h\varphi\ \mid\ \Diamond_v\varphi,
\]
%
where $p$ ranges over propositional variables.
For Kripke complete modal logics $L_h$ and $L_v$, we define their \emph{product logic}\index{product logic} by taking
\begin{equation*}
L_h\times L_v\ := \{\mbox{bimodal formula $\varphi$}\mid\
\mbox{$\varphi$ is valid in $\F_h\times \F_v$, where $\F_i$ is a frame for $L_i$, $i=h,v$}\}.
\end{equation*}

%****

\subsection{Modal product logics -- some known results}


Unlike for unimodal logic, and for logics having multiple non-interacting modal operators --- known as \emph{fusions} ---
 there is a scarcity of general results for logics with interacting modal operators, and for products of modal logics, in particular. Below we summarise some of the known results that are relevant to the
 thesis.
 
 As concerns the decision problem, it is known that product logics of the form $L\times\K$ and $L\times \Sfive$ are typically decidable, where $\K$ and $\Sfive$ are the respective logics of all frames
and all equivalence relations \cite{GKWZ03}. On the other hand, if both component logics are determined by \emph{transitive} frames of unbounded depth (such as, say, $\Kfour$, the logic of all transitive relations), then the product logic is
typically undecidable \cite{MarxReynolds1999,ReynoldsZakharyaschev2001,Gabelaia2005}.

 As concerns axiomatisations, product logics always validate the \emph{commutativity} and \emph{Church--Rosser} (\emph{confluence}) axioms, describing certain interactions between the two modalities. Hence, in considering possible axiomatisations for $L_h\times L_h$, it is a good starting point to consider the \emph{commutator}
$[L_h,L_v]$: the smallest bimodal logic containing both $L_h$, $L_v$ and the aforementioned interactions.
The general result of Gabbay and Shehtman \cite{GabbayShehtman1998} says that if both
component logics $L_h$ and $L_v$ are characterised by \emph{Horn-definable} classes of frames, then they
are \emph{product matching}:  their product $L_h\times L_v$ coincides with their commutator $[L_h,L_v]$ (and so the product logic is finitely axiomatisable whenever both component
logics are such). However, not much have been known about axiomatising product logics outside the scope
of this result. For example, $\K\times\Kfourt$ is known to be non-finitely axiomatisable \cite{Kurucz2012}, where  $\Kfourt$ denotes the (non-Horn definable) logic of all linear orders.


 
 %***
 
 \subsection{Difference logic}
 

%
 Von~Wright's `logic of elsewhere' \cite{vonWright1979}, denoted by $\Diff$ in the thesis, is characterised by the class of all difference relations, in which every possible world is accessible from every other \emph{distinct} possible world. So $\Diff$-frames are only slightly `richer' in expressive power than the universal frames
 characterising $\Sfive$. $\Diff$ is the underlying logic of the \emph{difference operator}~\cite{deRijke1992,deRijke1993}, and is connected to \emph{nominals}~\cite{Gargov1990} and \emph{graded modalities}~\cite{Areces2010}, all of which find many applications in description logics~\cite{Baader2003}.
It is also a simple example of a non-Horn definable modal logic. 
$\Diff$ is a finitely axiomatisable and decidable unimodal logic, having the finite model property.
The {\sc coNP} complexity of its decision problem coincides with that of $\Sfive$.
%
The product logic $\Diff\times\Diff$ is related to the two-variable, substitution and equality-free fragment of first-order logic with counting quantifiers $\exists_{>1} x, \exists_{>1} y$.
%

%***

\subsection{Problems tackled in the thesis}


In the thesis
we study axiomatisation and decision problems of modal product logics of the form $L\times\Diff$
and related formalisms, such as one-variable modal and temporal logics with counting and with 
various domain assumptions, and extensions with a modal constant representing equality of the coordinates in the 2D models.
We make a case that,
despite the similarities that $\Diff$ shares with $\Sfive$ (in the structure of their frames, their computational complexity and their axiomatisations), their respective interactions in two dimensions might differ considerably. This is in sharp contrast with what happens in the two-variable fragment
of classical first-order logic, where the addition of `elsewhere' quantifiers and/or equality has no influence on the
complexity of the decision problem \cite{Gradel1997,GradelOttoRosen1997,PacholskiST00}.
The contributions of the thesis may also  serve as a case study to better steer investigation into more general principles governing the interactions between modal logics.

\comment{
For multimodal logics $L$, discussed herein, we will be chiefly concerned with the following types of problems:
\begin{itemize}
        \item The \emph{axiomatisation problem}: Is it possible to characterise the theorems of $L$ by means of some explicit (recursive or even finite) set of axioms, closed under the usual deduction rules of Modus Ponens, Necessitation and Uniform Substitution? A necessary condition for axiomatisability is that the theorems of $L$ are recursively enumerable.
        
		\item The \emph{decision problem}: Is a given formula $\phi$ valid in every $L$-model; 
that is to say, is $\phi$ a theorem of $L$?
Dually, we have the \emph{satisfiability problem}: Is a given formula $\phi$ satisfiable is some $L$-model? 
We will be interested in the computational (worst case) complexity of the decision / satisfiability problem.

         \item The \emph{finite frame problem}: Is a given finite frame $\F$ a frame for $L$? 
         If $L$ is finitely axiomatisable, then this problem is trivially decidable, however this need not be true in general. This problem can often be reduced to the decision problem, but can, in many cases, be genuinely simpler.
		
	\item The \emph{finite model property}: Is every non-theorem of $L$ refutable in some finite frame for $L$? 
		 An affirmative answer to this question provides us with an effective procedure by which we can enumerate all non-theorems of $L$, whenever the finite frames problem for $L$ is decidable. This is the case when $L$ is finitely axiomatisable,
		 in which case the theorems of $L$ are also recursively enumerable. Hence, every finitely axiomatisable logic with the finite model property is decidable.
	%
	\end{itemize}
}


%*************************************************

\section{Thesis Summary}

The thesis is divided into three parts, together with an appendix: 
%
Part I, comprising Chapters~2--4, serves as a general introduction and provides the necessary background and definitions for the following topics. %topics contained therein. 
%
In Part II, comprising Chapters~5--8, we consider problems relating to the axiomatisation and computational properties of various two-dimensional modal 
product logics involving the difference relation. 
% 
Part III, that is Chapter 9--10, goes beyond the regular product construction to consider various \emph{relativisations} of products, and products equipped with a \emph{diagonal element}, mirroring the addition of an `equality' operator connecting the two dimensions by their common elements. 
%
Throughout what follows, section and theorem numbers will be given as they appear in the thesis.

\newcommand{\theoremnumber}[2]{\setcounter{section}{#1}\setcounter{theorem}{#2-1}}

\subsection*{Chapter 5: Axiomatisation of Products}
Here we tackle problems relating to the axiomatisation and finite frame problems%
%
\footnote{The \emph{finite frame problem} for $L$ asks: given a finite frame,
is it a frame for $L$? This is always reducible to the decision problem of $L$, and also,
this is obvious when $L$ is finitely axiomatisable.}
 %
of logics of the form $L\times\Diff$.
In Section~5.1, we show that, unlike logics of the form $L\times\Sfive$ for Horn-definable $L$, product logics
with the non-Horn definable logic $\Diff$ are often not finitely axiomatisable:


\theoremnumber{5}{1}
\begin{theorem}\label{thm:nonfinax}
Let $L$ be any bimodal logic such that 
\begin{equation*}
	\K\times\wKfive \subseteq L \qquad\qquad \mbox{and} \qquad\qquad 
	(\bbZ,\bbZ^2)\times (\bbZ,\not=) \mbox{ is a frame for $L$}.
\end{equation*}
%\begin{itemize}
%	\item $\K\times \wKfive \subseteq L$,
%	\item $(\bbZ,\bbZ^2)\times (\bbZ,\not=)$ is a frame for $L$.
%\end{itemize}
Then $L$ cannot be axiomatised using only finitely many variables\index{non-finitely axiomatisable}\index{finitely axiomatisable}.%
\footnote{Here $\wKfive$ denotes a sublogic of $\Diff$, characterised by the class of all relations $R$ that are \emph{weakly-Euclidean\/}: $\forall x\forall y\forall z\big(x R y \land x R z \to y R z \lor y=z\big)$.}.
\end{theorem} 

%In Section~5.2 we show that the logic characterised by only \emph{square} product frames for $\Diff\times\Diff$ cannot be finitely axiomatised over $\Diff\times\Diff$; itself not finitely axiomatisable (Theorem~5.7).

\comment{
\theoremnumber{5}{7}
\begin{theorem}
\label{thm:nonfinax2}
Let $L$ be any bimodal logic such that
\begin{itemize}
	\item $\Diff\sqtimes\Diff \subseteq L$,
	\item $(\bbZ,\bbZ^2)\times (\bbZ,\not=)$ is a frame for $L$.
\end{itemize}
Then $L$ cannot be axiomatised over $\Diff\times\Diff$ using only finitely many variables.
\end{theorem}
}

In Section~5.3, we discuss products falling outside the aforementioned interval of logics and show that $\Alt$ and $L$ are product matching, whenever $L$ is any \emph{canonical} logic, and where $\Alt$ is the logic of all functional relations (Theorem~5.13). 
%
In particular, $\Alt\times\Diff=[\Alt,\Diff]$ and $\Alt\times\Kfourt=[\Alt,\Kfourt]$. Consequently, both $\Alt\times\Diff$ and $\Alt\times\Kfourt$ are finitely axiomatisable. 
 These are, therefore, the first known examples of products that coincide with the respective commutators, whose components are not both Horn-definable. 

\medskip
In Section~5.4, we turn our attention to $\Sfive\times\Diff$ and provide a \emph{full structural characterisation} of the finite frames for $\Sfive\times\Diff$ (Theorem~5.22), thereby providing a polynomial time algorithm for its finite frame problem, despite being non-finitely axiomatisable by Theorem~5.1 and its decision problem
being {\sc coNExpTime}-complete.

\theoremnumber{5}{23}
\begin{theorem}
\label{thm:finiteframe}
	The finite frame problem for $\Sfive\times\Diff$ is decidable in polynomial time.
\end{theorem}

We conclude, in Section~5.5, with a discussion of how the characterisation theorem may be generalised to describe the finite frames 
for $\Diff\times\Diff$.

\subsection*{Chapter 6: Finite Model Properties}
%In Chapter~6, we discuss various versions of the finite model property (fmp) of bimodal logics related to products of the
%form $L\times\Diff$.
As product logics are `semantically' defined, there can be other `non-standard' (that is, \emph{non-product})
frames validating a product logic. This is why it is meaningful to consider different versions of the
finite model property (fmp) in this context. A product logic $L_h\times L_v$
%
\begin{itemize}\itemsep=-1pt
\item
has the \emph{fmp} if every $\varphi\notin L_h\times L_v$ fails in a finite frame for $L_h\times L_v$;
\item
has the \emph{product fmp} if every $\varphi\notin L_h\times L_v$ fails in a finite product frame for $L_h\times L_v$;
\item
has the \emph{square product fmp} if every $\varphi\notin L_h\times L_v$ fails in a finite product frame $\F_h\times\F_v$ for $L_h\times L_v$, where $\F_h$ and $\F_v$ have the same cardinality.
\end{itemize}
%
In Chapter~6 we discuss these properties related to products of the
form $L\times\Diff$.

In Section~6.1 we generalise the result of \cite{GradelOttoRosen1997} saying that the two-variable first-order fragment with
counting lacks the finite model property:

%prove the following result: %show that no normal extension of $[\wKfive,\wKfive]$, having $(\omega,\not=)\times(\omega,\not=)$ among its frames, can be characterised by its finite frames. %In particular, it follows that $\Diff\times\Diff$ lacks the finite model property. 

\theoremnumber{6}{3}
\begin{theorem}\label{thm:nofmp}
Let $L$ be any bimodal logic such that:
\begin{equation*}
	[\wKfive,\wKfive] \subseteq L \qquad\qquad \mbox{and} \qquad\qquad 
	(\omega,\not=)\times (\omega,\not=) \mbox{ is a frame for $L$.}
\end{equation*}
%\begin{itemize}
%	\item $[\wKfive,\wKfive] \subseteq L$,
%	\item $(\omega,\not=)\times (\omega,\not=)$ is a frame for $L$.
%\end{itemize}
	Then $L$ does not possess the fmp.
\end{theorem}

In particular, it follows that neither $[\Diff,\Diff]$ nor $\Diff\times\Diff$ can be characterised by their finite frames. 
%
%Note that while the product logic $\Diff\times\Diff$ is characterised by the class of all products of difference frames, there may be many more \emph{non-product} frames that validate all the axioms of $\Diff\times\Diff$. Hence, this result is more general than saying that $\Diff\times\Diff$ is not characterised by its finite product frames --- a result that follows easily from the lack of finite model property for the two-variable fragment of first-order logic with counting quantifiers. 
%
We note that $\Diff\times\Diff$ is, therefore, one of the simplest examples of a non-finitely axiomatisable logic without the fmp, that is nonetheless decidable. (The latter can be shown by an easy reduction to 
two-variable first-order logic with counting.)
%\footnote{It should be noted that neither the finite model property nor a finite axiomatisation, alone, is sufficient to guarantee decidability~\cite{Urquhart1981,Isard1977}, nor are they both necessary~\cite{Gabbay1971,HughesCresswell1975,Cresswell1979}.}.
%We note that $\Diff\times\Diff$ is, therefore, among the simplest examples of a decidable logic that lacks both the fmp and any finite axiomatisation

On the other hand,
in Section~6.2 we show the following, by a variation on the well-known quasimodel technique
\cite{GKWZ03}:
%that $\Sfive\times\Diff$ does have the finite model property, even with respect to its product frames. 

\theoremnumber{6}{6}
\begin{theorem}
\label{thm:S5xDiffpfmp}
	$\Sfive\times\Diff$ has the exponential product fmp.
\end{theorem}

However, there is a fine line: It is shown that any logic extending $\Sfive\times\Diff$ does not enjoy the square product fmp (Theorem~6.1).
%
These results show that, despite their similarities, the products of $\Sfive$ and $\Diff$ behave very differently with regards to their finite model properties, as summarised below in Table~\ref{tab:squarelogics}.

\begin{table}[ht!]
\begin{center}
\begin{tabular}{|r|c|c|c|}
\hline
& $\Sfive\times\Sfive$ & $\Sfive\times\Diff$ & $\Diff\times\Diff$\\
\hline
 fmp & \cmark & \cmark & \xmark\\
product fmp & \cmark & \cmark & \xmark\\
square product fmp & \cmark & \xmark & \xmark\\
\hline
\end{tabular}
\end{center}
\caption{Comparison of finite model properties for products of $\Sfive$ and $\Diff$.}
\label{tab:squarelogics}
\end{table}

\subsection*{Chapter 7: Computational Complexity of Commutators}
In Chapter~7, we discuss some decision problems of commutators involving $\Diff$. 
%
No techniques have been known for
handling the decision problems of commutators that do not coincide with their respective products.
In particular, the decidability and complexity of the decision problems for both $[\Diff,\Diff]$ and $[\Sfive,\Diff]$ have been unsolved (by Theorem~\ref{thm:nonfinax}, neither of these logics coincide with their respective products). 

In Section~7.1, we introduce a novel approach to obtaining decidability results for the decision problems for each of these logics, together with elementary upper bounds on their respective complexities. 
We achieve these results with the aid of a recursive satisfiability-preserving reduction of each commutator to their corresponding product logic.

\theoremnumber{7}{1}
\begin{theorem}
\label{thm:commdec}
 The decision problems for both $[\Sfive,\Diff]$ and $[\Diff,\Diff]$ are decidable.
\end{theorem}

By a variation on this technique, we are able to show that, unlike $[\Diff,\Diff]$ (Theorem~6.3), 
$[\Sfive,\Diff]$ \emph{is} characterised by its finite frames (Theorem~7.12).


\subsection*{Chapter 8: Computational Complexity of Products}


Product frames are always `grid-like' by definition. Hence, in those cases where coordinate-wise `universal' and `next-time' operators are both available, it becomes straightforward to obtain lower bounds by using reductions from various complex grid-based problems, such as tiling problems or Turing machine problems.
For example, it is easy to see that the decision problem for $\Ku\times\K_u$ is undecidable \cite{Marx1999}, where $\Ku$ is the logic $\K$ of all frames enriched with the \emph{universal modality\/}.

Previous lower bound proofs \cite{MarxReynolds1999,ReynoldsZakharyaschev2001,Gabelaia2005}
on product logics over \emph{transitive} frames overcome the lack of next-time operators by `diagonally' encoding the $\omega\times\omega$-grid in product models. Here we develop a novel technique, making direct use of the grid-like structure of product frames, to obtain undecidability results for a host of product logics using reductions from various (\emph{Minsky}) \emph{counter machine} problems.
%These results stand in sharp contrast with the corresponding results for logics of the form $L\times \Sfive$, whose decision problems are all known to be decidable.

In particular, in Section~8.2 we introduce the technique for cases when a `horizontal' next-time operator is still available.
We show that $\Ku\times\Diff$ is undecidable but still recursively enumerable (Corollary~8.4). 
%
Furthermore, a variation on this technique shows the product of $\Diff$ and 
$\K$ enriched with the \emph{common knowledge} (\emph{transitive closure}) operator is not even analytic. The same is true of $\Ptl\times\Diff$, in which $\Ptl$ denotes the bimodal (with next-time and `eventually') \emph{propositional temporal logic} over the natural numbers
%characterised by the frame $(\omega,S,<)$, where $S$ is the successor relation on $\omega$ 
(Corollary~8.6). 



%\theoremnumber{8}{6}
%\begin{corollary}
%	The decision problems for $\Kc\times\Diff$ and $\Ptl\times\Diff$ are both $\Pi^1_1$-hard.
%\end{corollary}

In Section~8.3 we sharpen this technique and discuss products of the form $L\times\Diff$, where $L$ is a unimodal logic (without next-time, with the sole temporal operator `eventually') characterised by some class \emph{linear orders\/}.
%We prove that the decision problem is undecidable whenever $\C$ comprises any class of linear orders containing $(\omega,<)$, and highly undecidable whenever $\C$ comprises any class of modally discrete linear orders containing $(\omega,<)$, or any class of Noetherian linear orders containing $(\omega+1,>)$ (Theorems~8.8, 8.14 and 8.31). 
%
In particular, we prove that $\Kfourt\times\Diff$ is undecidable, while for logics $L$ determined by various \emph{discrete}
linear orders $L\times\Diff$ is highly undecidable (Corollaries~8.13, 8.16 and 8.36). 
%
%We show, too, that $\Log(\C\times\Fr\Diff)$ is $\Pi^0_1$-complete when $\C$ comprises the class of all \emph{finite} strict linear orders (Theorem~8.19).

% Note that it remains open whether the technique can be extended to cases of `branching', transitive frames (without next-time). In particular, it is unknown whether the product of $\Diff$ and the logic $\Kfour$ of all transitive frames is decidable.
 

Finally, in Section~8.4 we show that the results of Section~8.3 are genuine generalisations of the undecidability results obtained in \cite{MarxReynolds1999,ReynoldsZakharyaschev2001}, by giving a polynomial reduction from the decision problem for $L\times\Diff$ to that of $L\times\Kfourt$, whenever $L$ is Kripke complete. Note that
despite the shared \co\NP-completeness of the decision problems for both $\Kfourt$ and $\Diff$, one cannot hope for a general reverse reduction, since $\Diff\times\Diff$ is decidable, while $\Diff\times\Kfourt$ is undecidable. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\subsection*{Chapter 9: First-order Modal Logics and Relativised Products}
In Chapter~9, we explore a variation on the standard product construction, motivated by connections between modal product logics of the form $L\times\Sfive$ and 
$L\times\Diff$, and various one-variable fragments of first-order modal logics. 
%
Owing to philosophical debate as to how we should interpret statements involving both modal operators and first-order quantifiers, investigation into such first-order modal logics has motivated a range of possible semantics, including those in which the domain of interpretation is permitted to either \emph{expand} or \emph{contract}, relative to the direction of the modal accessibility relation. 
%
This motivates the consideration of \emph{relativised product logics}, characterised by subframes of product frames that, similarly, expand or contract with respect to a given dimension.


It is easy to see that the decision problems of both expanding and contracting relativised products are reducible to that of the standard products. Here, we investigate whether they can be genuinely simpler. 
%
We show, first, that when $L$ is characterised by any class of strict linear orders, the decision problem for $L\dtimes\Diff$ over \emph{decreasing} domains has the same complexity as that of its `constant domain' counterpart $L\times\Diff$~(Theorems~9.6 \& 9.8).
%The following theorem of Section~9.4 indicicates the products with contracting domains can often as complex as their non-relativised counterparts, with respect to their decision problems.

\comment{\theoremnumber{9}{5}
\begin{proposition}[Hampson--Kurucz \cite{Hampson2015}]
\label{prop:decreduction}
	Let $\C$ be any class of modally discrete strict linear orders. Then 
	\begin{equation*}
	\phi\in \Log(\C\times \Fr\Diff) \qquad \iff \qquad \Bv^+\Bh^+(\Dh\top \to \Bv\Dh \top) \to \phi\in \Log(\C\dtimes \Fr\Diff).
	\end{equation*}
\end{proposition}

Furthermore, by adapting the proof of Theorem~8.8, we are able to show tha this result extends to decreasing products such as $\Kfourt\dtimes\Diff$, whose first component is not characterised by modally discrete frames (Theorem~9.8).
}

In Section~9.5, we turn our attention to relativised products with \emph{expanding} domains. We employ the techniques described in Section~8.3, with the aid of \emph{unreliable} counter machine problems, to obtain lower complexity bounds for various expanding product logics. In particular, we show that over expanding domain models, the decision problem for $L\etimes\Diff$ is non-elementary, whenever $L$ is determined by any class of strict linear orders containing $(\omega,<)$ and is even non-primitive recursive, when $L$ is the logic of all \emph{finite} linear orders (Theorems~9.9 \& 9.21, respectively). 

These lower bounds are notably weaker than those respective lower bounds obtained in Section~8.3, owing to the lower complexity of unreliable counter machines compared with their reliable counterparts. However, we demonstrate several cases where these bounds are optimal, via model-level reductions to known results. 
%
These results are summarised, below, in Table~\ref{tab:linear}.

%\theoremnumber{9}{9}
%\begin{theorem}
%\label{thm:K43xDiffexp}
%Let $\C$ be any class of strict linear orders such that $(\omega,<)\in \C$. Then the decision problem for $\Log(\C\etimes\Fr\Diff)$ is non-elementary.
%\end{theorem}

%\theoremnumber{9}{13}
%\begin{corollary}
%	The decision problem for $\Kfourt\etimes \Diff$ is non-elementary.
%\end{corollary}

%\theoremnumber{9}{15}
%\begin{theorem}[Hampson--Kurucz~\cite{Hampson2015}]
%\label{thm:LogNxDiffexp}
%	Let $\C$ be any class of modally discrete strict linear orders such that ${(\omega,<)\in \C}$. Then the decision problem for $\Log(\C\etimes\Fr\Diff)$ is $\Sigma^0_1$-hard.
%\end{theorem}

%\theoremnumber{9}{18}
%\begin{theorem}
%\label{thm:LogNxDiffexpUpp}
%	The decision problem for $\Log((\omega,<)\etimes \Fr\Diff)$ is $\Sigma^0_1$-complete.
%\end{theorem}

%\theoremnumber{9}{21}
%\begin{theorem}[Hampson--Kurucz~\cite{Hampson2015}]
%\label{thm:FinxDiffexp}
%	Let $\C$ be the class of all finite strict linear orders. Then the decision problem for $\Log(\C\etimes\Fr\Diff)$ is \Ackermann-hard.
%\end{theorem}

%\theoremnumber{9}{23}
%\begin{theorem}
%\label{thm:FinxDiffexpUpp}
%Let $\C$ be the class of all finite linear orders. Then the decision problem for $\Log(\C\etimes \Fr\Diff)$ is decidable.
%\end{theorem}


\begin{table}[ht!]
\begin{center}
\footnotesize
\begin{tabular}{l||c|c|c|c|}
%&&&&\\
& $(\omega,<)$ & all finite strict & all strict & all modally discrete \\
& & linear orders & linear orders & strict linear orders\\
\hline\hline
%&&&&\\[-5pt]
full & $\Pi_1^1$-complete &  $\Pi_1^0$-complete & $\Sigma^0_1$-complete &  $\Pi_1^1$-hard\ \unknown\\
\ \ products & {\scriptsize Theorem~8.14} & {\scriptsize Theorem~8.19} & {\scriptsize Theorem~8.8} & {\scriptsize Theorem~8.14}\\[3pt]
\hline
%&&&&\\[-5pt]
decreasing & $\Pi_1^1$-complete & $\Pi_1^0$-complete & $\Sigma^0_1$-complete & $\Pi_1^1$-hard\ \unknown\\
\ \ products & {\scriptsize Theorems~9.6 \&~9.2} & {\scriptsize Theorems~9.6 \&~9.2} & {\scriptsize Theorems~9.8 \&~9.2} & {\scriptsize Theorem~9.6}\\[3pt]
\hline
%&&&&\\[-5pt]
expanding &  $\Sigma^0_1$-complete & decidable\ \unknown & non-\Elem, r.e. & $\Sigma^0_1$-hard\\[1pt]
\ \ products & {\scriptsize Theorems~9.6 \&~9.2} & \Ackermann-hard & \framebox{decidable?} & \framebox{r.e.?}\\
&   & {\scriptsize Theorems~9.21 \&~9.23} & {\scriptsize Theorem~9.9} & {\scriptsize Theorem~9.6}\\[3pt]
\hline
\end{tabular}
\end{center}
\caption{Complexity of $L\times \Diff$ for various classes of linear orders and domain assumptions.}
\label{tab:linear}
\end{table}

\subsection*{Chapter 10: Products with a `Diagonal' Operator}
A well-know variation on the standard translation identifies the product logic $\Sfive\times\Sfive$ with the two-variable, equality- and substitution-free fragment of first-order logic. In this modal setting, equality can be modelled with an additional modal constant, denoted by $\delta$, interpreted in square frames as the identity (`\emph{diagonal}') relation.  (Note that $\delta$ is denoted by $d_{01}$ in the
 \emph{cylindric algebraic} algebraisation of two-variable first-order logic \cite{Henkin1971}).
%
In Chapter 10, we consider the decision problems of products of \emph{arbitrary} modal logics expanded with this additional feature.
%
We show that, unlike the two-variable fragment whose validity problem remains \co\NExpTime-complete, both with and without equality \cite{Scott1962,Mortimer1975,Gradel1997},
the addition of a dimension-joining diagonal constant can often lead to a considerable increase in the complexity of product~logics.

In Section~10.2, we first establish a connection between $\delta$-products and regular products. In particular, we show that the \emph{global consequence problem} for certain product logics can be reduced to the decision problem for their respective $\delta$-products (Theorem~10.3). This provides us with undecidable lower bounds for several $\delta$-product logics whose $\delta$-free counterparts are decidable. In particular, we show that the decision problems for the $\delta$-products $\K\dprod \Kfour$ and $\K\dprod \K$  are undecidable~(Corollary~10.4).

The latter result has a surprising consequence:
In~\cite{GabbayShehtman2000}, the authors introduce the \emph{simple square fragment} $\SF$ as the decidable fragment of first-order logic, whose formulas are constructed from atomic binary predicates $P_i(x,y)$, by freely applying Boolean connectives and relativised quantifiers of the form
\begin{equation*}
	\exists z \big(R(x,z) \land \phi(z,y)\big) \qquad \mbox{and} \qquad  \exists z \big(R(y,z) \land \phi(x,z)\big) ,
\end{equation*}
where $R$ is a built-in binary predicate.
%
Unlike the decidable two-variable fragment of first-order logic, $\SF$ is situated within the undecidable three-variable fragment~\cite{Suranyi1943,Suranyi1950}.
%
This simple square fragment is readily seen to be the image of propositional bimodal formulas under the following variation of the standard translation $(\cdot)^\ast$,
\begin{gather*}
	p_j^\ast = P_j(x,y), \quad \mbox{for $p_i\in \prop$}, \qquad (\neg\psi)^\ast = \neg \psi^\ast, \qquad
	(\psi_1\land \psi_2)^\ast = \psi_1^\ast \land \psi_2^\ast,\\[5pt]
	(\Dh\psi)^\ast = \exists z \big(R(x,z) \land \psi^\ast(z/x,y)\big) , \qquad (\Dv\psi)^\ast = \exists z \big(R(y,z) \land \psi^\ast(x,z/y)\big) ,
\end{gather*}
where $R,P_0, P_1, \dots$ are binary predicate symbols. 
%
Moreover, it is straightforward to check that $\phi$ is a theorem of $\K\times\K$ if and only if $\phi^\ast$ is a theorem of first-order logic. Since the decision problem for $\K\times \K$ is 
decidable~\cite{GabbayShehtman1998}, so too must be the simple square fragment $\SF$.
%
However, by expanding $\SF$ to include equality, the fragment $\SFeq$ is readily seen to be the image of the bimodal language with the $\delta$-constant under the extension of $(\cdot)^\ast$ that interprets $\delta^\ast = (x \eq y)$. It is similarly straightforward to check that $\phi$ is a theorem of $\K\dprod\K$ if and only if $\phi^\ast$ is a theorem of first-order logic with equality. 
%
Hence, it follows from Corollary~10.4 that the simple square fragment \emph{with equality} is undecidable. 
%
Thus $\SFeq$ joins the \emph{G\"odel class}~\cite{Godel1933,Goldfarb1984} as yet another example of an undecidable fragment of first-order logic, whose equality-free fragment is decidable.

\smallskip
It turns out 
that there are limitations to the approach of Section~10.2. Indeed, there are cases where such a reduction is either unhelpful or demonstrably non-existent.  For example, while the global consequence problem for $\K\times\Sfive$ is reducible to the decision problem for $\Pdl\times\Sfive$ (for Propositional Dynamic Logic $\Pdl$), and is thus decidable in \co\NkExpTime{2}~\cite{Wolter2000,Schmidt2003}, the decision problem for $\K\dprod\Sfive$ is shown to be undecidable (Theorem~10.7). 
%
Furthermore, we show in Section~10.5 that the decision problem for $\K\dprod\Alt$ can be decided in \co\NExpTime, whereas the undecidability of the global consequence problem for $\K\times\Alt$ can be established by a straightforward reduction from the unconstrained tiling problem~\cite{vanEmdeBoas1997}.




In Section~10.3, we introduce the notion of computation by means of \emph{faulty approximations} as a novel variation on unreliable counter machines. Unlike lossy and incrementing counter machines, we show that computation by faulty approximation is \emph{Turing-complete}. 
%
In Section~10.4.1, we exploit the greater flexibility of this new formalism to obtain undecidable lower bounds for a host of $\delta$-products, using a variation on the techniques described in Chapter~8. Among which, we show that the decision problem for $\K\dprod\Sfive$ is undecidable, despite the decidability of both the decision problem and the global consequence problem~for~$\K\times\Sfive$.
This is a surprising `jump' from the {\sc coNExpTime}-complexity of
$\K\times\Sfive$ \cite{Marx1999}.



%\theoremnumber{10}{11}
%\begin{corollary}
%The decision problem for $\K\dprod\Sfive$ is $\Sigma^0_1$-complete.
%\end{corollary}

%One immediate corollary of this is that the delta-products $\K\dprod\K$ and  $\K\dprod\Sfive$ is undecidable, despite the relatively modest \co\NExpTime-completeness of their delta-free counterparts.



In Section~10.4.2, we extend this technique to $\delta$-products in which the first component is characterised by some class of linear orders. In particular, we show that the decision problems for $\Kfourt\dprod \K$ and $\Kfourt\dprod \Sfive$ are both undecidable (Theorem~10.12). (Note that the complexity of the global consequence problem for $\Kfourt\times\K$ is not known, while that of $\Kfourt\times\Sfive$ is known to be decidable~\cite{Reynolds1997}.)

\smallskip
We conclude Chapter~10 by probing the limitations of the above counter machine approach and show that the $\delta$-product $\K\dprod\Alt$ --- lying outside the remit of the aforementioned results --- has the exponential product fmp and is thus decidable (Theorem~10.17).


\setcounter{section}{2}
\section{Conclusion}

The thesis leaves open several questions throughout that serve as directions for future research endevours; some have, since publication of this thesis, been successfully addressed such as Questions~6.13--6.14, regarding the product fmp and computational complexity of the decision problem for $\K\times\Diff$. This was addressed in~\cite{Hampson2016}, in which $\K\times\Diff$ was shown to share the same \co\NExpTime-completeness as $\K\times\Sfive$. Indeed, a stronger result was shown: that the one-variable fragment of first-order modal logic $\K$ with \emph{arbitrary} counting quantifiers, denoted $\mathbf{Q^{\#}K}$, is \co\NExpTime-complete~\cite{Hampson2016} --- the product logic $\K\times\Diff$ being a syntactic variant of the special case in which we allow only the counting quantifiers~$\exists_{>0} x$~and~$\exists_{>1} x$.

%\begin{theorem}[Hampson]
%	The one-variable fragment of $\mathbf{Q^{\#}K}$ is \co\NExpTime-complete.
%\end{theorem}

Another forthcoming result concerns the axiomatisation of $\Sfive\times\Diff$, shown to be non-finitely axiomatisable in Theorem~5.1. Based on the characterisation of its frames given in Section~5.4, we were able to construct an infinite \emph{explicit} axiomatisation for $\Sfive\times\Diff$~\cite{Hampson2017}.  This is the first explicit axiomatisation for a non-finitely axiomatisable product logic that is `orthodox' in the sense that it uses only the `traditional' rules of Substitution, Modus Ponens and Necessitation, and does not use any `irreflexivity-type' rules.
%
Owing to the more intricate structure of the frames for $\Diff\times\Diff$ (see Section~5.5), an explicit
`orthodox' axiomatisation of the recursively enumerable product logic $\Diff\times\Diff$ remains subject to future work.





\footnotesize
\bibliographystyle{plain}
\bibliography{thesis}

\end{document}